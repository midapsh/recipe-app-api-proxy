# Adicionar uma versao na qual utilizamos um arquivo sem privilegio de root e com um Linux leve
FROM nginxinc/nginx-unprivileged:1-alpine

LABEL maintainer="maintainer@londonappdev.com"

COPY ./default.conf.tpl /etc/nginx/default.conf.tpl
COPY ./uwsgi_params /etx/nginx/uwsgi_params

ENV LISTEN_PORT=8000
ENV APP_HOST=app
ENV APP_PORT=9000

# Switch to root to make some changes
USER root

RUN mkdir -p /vol/static
RUN chmod 755 /vol/static
RUN touch /etc/nginx/conf.d/default.conf
RUN chown nginx:nginx /etc/nginx/conf.d/default.conf

COPY ./entrypoint.sh /entrypoint.sh
# Explicit says that the file is executable
RUN chmod +x /entrypoint.sh

# Switch back to default user
USER nginx

CMD [ "/entrypoint.sh" ]
