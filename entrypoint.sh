#!/bin/sh

# Return a failure everytime
set -e

# < Substituir env variables
# > Colocar em um novo local
envsubst < /etx/nginx/default.conf.tpl > /etc/nginx/conf.d/default.conf

# Start NGINX service
# It tells to nginx to run the application on the foreground because its a good practice in Docker to run the main application in this way
# This way, all the logs and outputs from our engine server get printed to the Docker output
nginx -g 'daemon off;'
